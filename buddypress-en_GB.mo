��          �            h     i     p     �     �     �  j   �       +   #  /   O  (     G   �  >   �  z   /  %   �  U  �     &     -     ?     K     X  i   e     �  +   �  /     (   ;  G   d  >   �  y   �  %   e                              	                                    
              Avatar Crop Group Avatar Edit Avatar Group Avatar Group avatar If you'd like to remove the existing avatar but not upload a new one, please use the delete avatar button. No Group Avatar The group avatar was uploaded successfully! The new group avatar was uploaded successfully. There was a problem cropping the avatar. There was an error saving the group avatar, please try uploading again. To skip the avatar upload process, hit the "Next Step" button. Upload an image to use as an avatar for this group. The image will be shown on the main group page, and in search results. Your avatar was deleted successfully! Project-Id-Version: BuddyPress 
Report-Msgid-Bugs-To: http://wppolyglots.wordpress.com
POT-Creation-Date: 2014-04-18 17:30:35+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2014-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
 Mascot Crop Group Mascot Edit Mascot Group Mascot Group mascot If you'd like to remove the existing mascot but not upload a new one, please use the delete button below. No Group Mascot The group mascot was uploaded successfully! The new group mascot was uploaded successfully. There was a problem cropping the mascot. There was an error saving the group mascot, please try uploading again. To skip the mascot upload process, hit the "Next Step" button. Upload an image to use as a mascot for this group. The image will be shown on the main group page, and in search results. Your mascot was deleted successfully! 